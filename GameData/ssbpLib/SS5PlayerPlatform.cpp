﻿// 
//  SS5Platform.cpp
//
#include "SS5PlayerPlatform.h"
#include "windows.h";

/**
* 各プラットフォームに合わせて処理を作成してください
* DXライブラリ用に作成されています。
*/
#include <Siv3D.hpp>

namespace ss
{
	/**
	* ファイル読み込み
	*/
	unsigned char* SSFileOpen(const char* pszFileName, const char* pszMode, unsigned long * pSize)
	{
		unsigned char * pBuffer = NULL;
		SS_ASSERT2(pszFileName != NULL && pSize != NULL && pszMode != NULL, "Invalid parameters.");
		*pSize = 0;
		do
		{
		    // read the file from hardware
			FILE *fp = fopen(pszFileName, pszMode);
		    SS_BREAK_IF(!fp);
		    
		    fseek(fp,0,SEEK_END);
		    *pSize = ftell(fp);
		    fseek(fp,0,SEEK_SET);
		    pBuffer = new unsigned char[*pSize];
		    *pSize = fread(pBuffer,sizeof(unsigned char), *pSize,fp);
		    fclose(fp);
		} while (0);
		if (! pBuffer)
		{

			std::string msg = "Get data from file(";
		    msg.append(pszFileName).append(") failed!");
		    
		    SSLOG("%s", msg.c_str());

		}
		return pBuffer;
	}

	/**
	* テクスチャの読み込み
	*/
	long SSTextureLoad(const char* pszFileName, SsTexWrapMode::_enum  wrapmode, SsTexFilterMode::_enum filtermode)
	{
		/**
		* テクスチャ管理用のユニークな値を返してください。
		* テクスチャの管理はゲーム側で行う形になります。
		* テクスチャにアクセスするハンドルや、テクスチャを割り当てたバッファ番号等になります。
		*
		* プレイヤーはここで返した値とパーツのステータスを引数に描画を行います。
		*/
		static long rc = -1;
		rc++;

		size_t pReturnValue;
		wchar_t wcstr[64];
		const char *mbstr;
		mbstowcs_s(&pReturnValue, wcstr, 64, pszFileName, _TRUNCATE);

		TextureAsset::Register(Format(rc), wcstr);

		return rc;
	}
	
	/**
	* テクスチャの解放
	*/
	bool SSTextureRelese(long handle)
	{
		TextureAsset::Release(Format(handle));

		return true;
	}

	/**
	* テクスチャのサイズを取得
	* テクスチャのUVを設定するのに使用します。
	*/
	bool SSGetTextureSize(long handle, int &w, int &h)
	{
		w = TextureAsset(Format(handle)).size.x;
		h = TextureAsset(Format(handle)).size.y;

		return true;
	}


	/**
	* スプライトの表示
	*/
	void SSDrawSprite(State state)
	{
		SSV3F_C4B_T2F_Quad quad;
		quad = state.quad;
		float cx = ((state.rect.size.width) * -(state.pivotX - 0.5f));
		float cy = ((state.rect.size.height) * -(state.pivotY - 0.5f));

		quad.tl.vertices.x += cx;
		quad.tl.vertices.y += cy;
		quad.tr.vertices.x += cx;
		quad.tr.vertices.y += cy;
		quad.bl.vertices.x += cx;
		quad.bl.vertices.y += cy;
		quad.br.vertices.x += cx;
		quad.br.vertices.y += cy;

		float t[16];
		TranslationMatrix(t, quad.tl.vertices.x, quad.tl.vertices.y, 0.0f);
		MultiplyMatrix(t, state.mat, t);
		quad.tl.vertices.x = t[12];
		quad.tl.vertices.y = t[13];
		TranslationMatrix(t, quad.tr.vertices.x, quad.tr.vertices.y, 0.0f);
		MultiplyMatrix(t, state.mat, t);
		quad.tr.vertices.x = t[12];
		quad.tr.vertices.y = t[13];
		TranslationMatrix(t, quad.bl.vertices.x, quad.bl.vertices.y, 0.0f);
		MultiplyMatrix(t, state.mat, t);
		quad.bl.vertices.x = t[12];
		quad.bl.vertices.y = t[13];
		TranslationMatrix(t, quad.br.vertices.x, quad.br.vertices.y, 0.0f);
		MultiplyMatrix(t, state.mat, t);
		quad.br.vertices.x = t[12];
		quad.br.vertices.y = t[13];

		quad.tl.colors.a = quad.tl.colors.a * state.Calc_opacity / 255;
		quad.tr.colors.a = quad.tr.colors.a * state.Calc_opacity / 255;
		quad.bl.colors.a = quad.bl.colors.a * state.Calc_opacity / 255;
		quad.br.colors.a = quad.br.colors.a * state.Calc_opacity / 255;

		state.sprite.vertices[0].set(quad.tl.vertices.x, quad.tl.vertices.y, quad.tl.texCoords.u, quad.tl.texCoords.v, Color(quad.tl.colors.r, quad.tl.colors.g, quad.tl.colors.b, quad.tl.colors.a));
		state.sprite.vertices[1].set(quad.tr.vertices.x, quad.tr.vertices.y, quad.tr.texCoords.u, quad.tr.texCoords.v, Color(quad.tr.colors.r, quad.tr.colors.g, quad.tr.colors.b, quad.tr.colors.a));
		state.sprite.vertices[2].set(quad.bl.vertices.x, quad.bl.vertices.y, quad.bl.texCoords.u, quad.bl.texCoords.v, Color(quad.bl.colors.r, quad.bl.colors.g, quad.bl.colors.b, quad.bl.colors.a));
		state.sprite.vertices[3].set(quad.br.vertices.x, quad.br.vertices.y, quad.br.texCoords.u, quad.br.texCoords.v, Color(quad.br.colors.r, quad.br.colors.g, quad.br.colors.b, quad.br.colors.a));

		state.sprite.draw(TextureAsset(Format(state.texture.handle)));

	}

	/**
	* ユーザーデータの取得
	*/
	void SSonUserData(Player *player, UserData *userData)
	{
		//ゲーム側へユーザーデータを設定する関数を呼び出してください。
	}

	/**
	* ユーザーデータの取得
	*/
	void SSPlayEnd(Player *player)
	{
		//ゲーム側へアニメ再生終了を設定する関数を呼び出してください。
	}


	/**
	* 文字コード変換
	*/ 
	std::string utf8Togbk(const char *src)
	{
		int len = MultiByteToWideChar(CP_UTF8, 0, src, -1, NULL, 0);
		unsigned short * wszGBK = new unsigned short[len + 1];
		memset(wszGBK, 0, len * 2 + 2);
		MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)src, -1, (LPWSTR)wszGBK, len);

		len = WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)wszGBK, -1, NULL, 0, NULL, NULL);
		char *szGBK = new char[len + 1];
		memset(szGBK, 0, len + 1);
		WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)wszGBK, -1, szGBK, len, NULL, NULL);
		std::string strTemp(szGBK);
		if (strTemp.find('?') != std::string::npos)
		{
			strTemp.assign(src);
		}
		delete[]szGBK;
		delete[]wszGBK;
		return strTemp;
	}

	/**
	* windows用パスチェック
	*/ 
	bool isAbsolutePath(const std::string& strPath)
	{
		std::string strPathAscii = utf8Togbk(strPath.c_str());
		if (strPathAscii.length() > 2
			&& ((strPathAscii[0] >= 'a' && strPathAscii[0] <= 'z') || (strPathAscii[0] >= 'A' && strPathAscii[0] <= 'Z'))
			&& strPathAscii[1] == ':')
		{
			return true;
		}
		return false;
	}

};
